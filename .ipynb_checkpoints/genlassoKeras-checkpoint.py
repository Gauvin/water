
 
import tensorflow as tf
from tensorflow import keras

from keras.layers import Input, Dense, Layer
from keras.models import Model
from keras.constraints import Constraint
from keras.regularizers import Regularizer

from keras import backend as K
from keras import regularizers


class ElementWiseMult(tf.keras.layers.Layer):

    """Custom keras layer => one parameter per pixel (and not pixel X pixel parameters)
    
    Performs matrix multiplication pixel by pixel vs standard matrix multiplication
    
    """
    
    def __init__(self,  kernel_regularizer=None, **kwargs):
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        
        print(self.kernel_regularizer)
        super(ElementWiseMult, self).__init__(**kwargs)
        

    def build(self, input_shape):

        print(f'Here is the dim{input_shape}')
        
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(int(input_shape[-2]),int(input_shape[-1])),
                                      initializer='uniform',
                                      trainable=True,
                                      regularizer=self.kernel_regularizer)
        self.output_dim= input_shape[1]
        
        
        super(ElementWiseMult, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        return tf.multiply(x,      self.kernel )
    
    def compute_output_shape(self, input_shape):
        return (input_shape[-1], self.output_dim)
    
    