---
title: "genlassoMtl"
author: "Charles"
date: "August 26, 2019"
output: html_document
---
 

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
library(miceadds)
library(glue)
library(sf)
library(raster)


library(viridis)
library(igraph)

library(neighRaster)
library(neighShp)
library(gradientvectorfield)
```
  

 
 
 
#Get the Mtl city elevation gradient norm raster by neighbourhood
```{r,message=F,echo=F,include=F}
 
city <- "Mtl"

listNeigh <-  c( "Pointe Saint-Charles",
                 "Saint-Henri",
                 "Ville-Émard/Côte Saint-Paul")
 
  
neighRastMtlElev <- neighRaster::neighRasterMtl$new( rasterPath = here::here("Data","GeoData","Raster",  "DEM", "cdem_dem_031H_croppedMtl.tif" ),
                                                  neighPath =here::here("Data","GeoData","MontrealNeighbourhoods"),
                                                  listNeigh = listNeigh,elevatrFallBack = T,zoomLevel = 14,
                                                  useBbox = T)
rastMtl <- neighRastMtlElev$getRaster() 



neighRastMtlNWDI <- neighRaster::neighRasterMtl$new( rasterPath =  here::here("Data","GeoData","Raster","NWDI","mtlMNWDICroppedReclass4326Boundary.tif"),
                                                   neighPath =here::here("Data","GeoData","MontrealNeighbourhoods"),
                                                   listNeigh = listNeigh,
                                                   useBbox = T)



myRastGradient <- neighRasterGradient$new(neighRaster = neighRastMtlElev,
                                          strFileName = "centralCroppedElev",
                                          rasterPathDirWrite = here::here("Data","GeoData","Raster", "GradientNorm","Montreal")
                                          )

rastGradientElev <- myRastGradient$getRaster()

rastNWDICropped <- neighRastMtlNWDI$getRaster()

rastGradientElev %>% plot
rastNWDICropped %>% plot

rastNWDICropped %>% values %>% unique
```


##genlasso

###Normalize raster first
```{r}

normalizeRaster <- function(rast){
  m <-  cellStats(rast,'mean')
  s <-  cellStats(rast,'sd')
  rastNorm <- calc(rast, fun = function(x)((x-m)/s))
  
  return(rastNorm)
}

rGradNorm <- rastGradientElev %>% normalizeRaster
rNWDINorm <- rastNWDICropped  #don't take the mean over binary values

rNWDINorm %>% values %>% unique()
```

####Aggregate
```{r}

aggFact <- 2

aggrRast <- function(rast,aggFact,fun=mean){
   aggregate(rast, fact=aggFact, fun=fun)
}

rGradNorm %<>% aggrRast(.,aggFact)
rNWDINorm %<>% aggrRast(.,aggFact,fun=max) #don't take the mean over binary values

rNWDINorm %>% values %>% unique()
```

###Impute missing + convert to matrix
```{r}

convertImpute <- function(rast){
  
  matNorm <- rayshaderDemo::convert_raster_to_matrix(rast)
  print(glue::glue("Replacing {matNorm %>% is.na %>% sum()} values with mean value"))
  matNorm[is.na(matNorm)] <- matNorm %>% mean(na.rm=T) 
  
  return(matNorm)
} 



matNWDI <- rayshaderDemo::convert_raster_to_matrix(rNWDINorm) #don't take the mean over binary values
matGradNorm <-  convertImpute(rGradNorm)
 
matNWDI %>% dim
matGradNorm %>% dim

matNWDI %>% as.numeric %>% unique
```


###Recover the data frame
```{r}


dfNWDI <- as.data.frame(rNWDINorm,xy=T) %>% rename(NWDI =mtlMNWDICroppedReclass4326Boundary)

rGradNormResampled <- raster::resample(rGradNorm, rNWDINorm,method="bilinear")  
dfGradientElev <- as.data.frame(rGradNormResampled,xy=T) %>% rename(gradElev=layer)


dfBothResampled <- full_join(dfNWDI, 
                             dfGradientElev, 
                             by=c("x","y")
                             )  
dfBothResampled %>% head
```

```{r}

fusedLassoDenoiser2DNWDI <- genlasso::fusedlasso2d(matNWDI,gamma=1)

```
```{r}

fusedLassoDenoiser2DNWDIGamma0 <- genlasso::fusedlasso2d(matNWDI,gamma=0)

```
```{r}

plotSubsetLassoPath <- function(fusedLassoResults, prob, dfBothResampled, respStr ){
  
  lambda <- fusedLassoResults$lambda %>% quantile(prob)
  beta <- coef(fusedLassoResults,
               lambda = lambda)$beta
  
  dfBothResampled$fit <- beta
  
  lambdaRound <- round(lambda,2)
  ggplot(dfBothResampled) + 
    geom_raster(aes(x=x,y=y,fill=fit)) + 
    ggtitle(glue::glue("Image denoising - {respStr}\nlambda: {lambdaRound}"))
}

plotFullImage <- function(respStr){
  
   ggplot(dfBothResampled) + 
    geom_raster(aes_string(x="x",y="y",fill=respStr)) + 
    ggtitle(glue::glue("Image denoising - {respStr}\nOriginal raster"))
  
}

plotAll <- function(fusedLassoResults, dfBothResampled, respStr){
  listNWDIPlots <- map(0:10/10, 
                       ~plotSubsetLassoPath(fusedLassoResults, .x, dfBothResampled, respStr)
                       )

  listPlots <- listNWDIPlots
  listPlots <- rlist::list.append(listPlots, plotFullImage(respStr)  )
  

  
  for(p in listPlots){
    (p) %>% show
  }
  
  return(listPlots)
}


```

```{r}

respStr <- "NWDI"
listPlots <- plotAll(fusedLassoDenoiser2DNWDI, dfBothResampled, respStr)
gridPlot <- cowplot::plot_grid(plotlist = listPlots)


ggsave(plot = gridPlot,
       filename = here::here("Figures","genlasso", glue::glue("genlasso2D{city}SudOuest{respStr}Boundary_agg{aggFact}.png")),
       device = "png",
       width = 20,
       heigh=15)

remove(fusedLassoDenoiser2DNWDI)
gc()
```
```{r}

respStr <- "NWDI"
listPlots <- plotAll(fusedLassoDenoiser2DNWDIGamma0, dfBothResampled, respStr)
gridPlot <- cowplot::plot_grid(plotlist = listPlots)


ggsave(plot = gridPlot,
       filename = here::here("Figures","genlasso", glue::glue("genlasso2DGamma0{city}SudOuest{respStr}Boundary_agg{aggFact}.png")),
       device = "png",
       width = 20,
       heigh=15)

remove(fusedLassoDenoiser2DNWDIGamma0)
gc()
```


```{r}

fusedLassoDenoiser2DGrad <- genlasso::fusedlasso2d(matGradNorm,gamma=1)

```


```{r}

 
respStr <- "gradElev"
listPlots <- plotAll(fusedLassoDenoiser2DGrad, dfBothResampled, respStr)
gridPlot <- cowplot::plot_grid(plotlist = listPlots)


ggsave(plot = gridPlot,
       filename = here::here("Figures","genlasso", glue::glue("genlasso2D{city}SudOuest{respStr}Boundary_agg{aggFact}.png")),
       device = "png",
       width = 20,
       heigh=15)

remove(fusedLassoDenoiser2DGrad)
gc()
```