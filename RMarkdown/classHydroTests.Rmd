---
title: "neighRiverClassTest"
author: "Charles"
date: "August 9, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
 

# Librairies
 
```{r, include=FALSE}

GeneralHelpers::loadAll()
library(tidyverse)
library(magrittr)
library(ggplot2)
 
#library(rayshader)
#library(rgdal)
#library(raster)
library(sf)
 
library(here)
library(glue)

library(Census2SfSp)
#library(rayshaderDemo) #made package from rayshader-demo from github

 

```

#Mtl
```{r}
mtNeighGetter <- neighShpMtl$new( here("Data","GeoData","MontrealNeighbourhoods"))
mtlRiverGetter <- hydroShpMtl$new()
mtlRiverNeighGetter <- neighHydroShpDefault$new(mtlRiverGetter,mtNeighGetter)
 
shpMtl <- mtNeighGetter$getShpNeigh()
 
mtlLakes <- mtlRiverGetter$getShpLakes()

mtlRiverNeighGetter$plotLakeAndNeigh(plotOnlyNeighIntersecting = F)
mtlRiverNeighGetter$plotLakeAndNeigh(plotOnlyNeighIntersecting = T)

```


##Mtl lakes
```{r}



mtlRiverGetter$getShpCatchment()
shpLakes <- mtlRiverGetter$getShpLakes()  

shpLakes %>% plot 

```

##Mtl island

```{r}



mtlComposite <- neighHydroShpMtl$new(
  here::here("Data","GeoData","Hydro","hydro_l" ),
                           here::here("Data","GeoData","Hydro","hydro_s" ),
                          here::here("Data","GeoData","MontrealNeighbourhoods"))

mtlComposite$getMtlIslandLand() %>% plot
```

```{r}

mtlComposite$plotAllWaterAndNeigh( plotOnlyNeighIntersecting = T)

```


```{r}

shpLakeInterMtl <- mtlComposite$getShpNeighIntersectLakes()

shpLakeInterMtl %>% plot

```




```{r}

shpRiversInterMtl <- mtlComposite$getShpNeighIntersectRivers()

shpRiversInterMtl %>% plot

```

```{r}

titleStr <- "Montreal neighbourhoods touching Canal Lachine and Canal de l'aqueduc"
mtlComposite$plotRiverAndNeigh(titleStr)

```



#Qc
```{r}

qcShpGetter <- neighShpQc$new( )
qcRiverGetter <- hydroShpQc$new()
qcRiverNeighGetter <- neighHydroShpDefault$new(qcRiverGetter,qcShpGetter)

shpQc <- qcShpGetter$getShpNeigh()
```

##Qc all lakes & rivers 


```{r}
qcComposite <- neighHydroShpQc$new()
qcComposite$plotLakeRiverAndNeigh(addNeighNames=F)

qcComposite$plotLakeRiverAndNeigh()

p1 <- qcComposite$plotLakeAndNeigh()
qcComposite$plotRiverAndNeigh(plotToCompose = p1)
```

```{r}

qcComposite <- neighHydroShpQc$new()
shpQcLakes <- qcComposite$getShpNeighIntersectLakes()

shpQcLakes %>% plot


shpQcRivers <- qcComposite$getShpNeighIntersectRivers()
shpQcRivers %>% plot

```

```{r}

qcComposite$plotAllWaterAndNeigh(plotOnlyNeighIntersecting = T)
qcComposite$plotAllWaterAndNeigh(plotOnlyNeighIntersecting = F)
```

```{r}


qcComposite$plotLakeRiverAndNeigh()
```

```{r}

qcComposite$plotLakeAndNeigh()

```

##St-Charles


```{r}

stCharlesComposite <- neighHydroShpSaintCharles$new()

```

###All neigh within the catchment
```{r}


shpStCharlesCatch <- stCharlesComposite$getShpNeighIntersectCacthment()

shpStCharlesCatch %>% plot

shpQc %>% filter(!NOM %in% shpStCharlesCatch$NOM) %>% plot

```


###Neigh touching
```{r}

stCharlesComposite$plotAllWaterAndNeigh()

```

```{r}

stCharlesComposite$plotLakeAndNeigh()

 

```

```{r}

stCharles <- hydroShpSaintCharles$new()
stCharles$getShpLakes() %>% plot

```

```{r}

stCharlesComposite$plotRiverAndNeigh()


```

###Default plotting
```{r}
stCharlesComposite$plotStCharlesAllCustom()

```