---
title: "reclassifyNWDI"
author: "Charles"
date: "September 12, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 

#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
library(miceadds)
library(glue)
library(sf)
library(raster)


library(viridis)
library(igraph)

library(neighRaster)
library(neighShp)
library(gradientvectorfield)
```
  


```{r}

#https://www.linkedin.com/pulse/ndvi-ndbi-ndwi-calculation-using-landsat-7-8-tek-bahadur-kshetri/

nrastQc <- neighRasterQc$new(  rasterPath =  here::here("Data","GeoData","Raster", "NWDI", "qcMNWDICropped4326.tif" ),
                    neighPath =here::here("Data","GeoData","QuebecNeighbourhoods"),
                    useBbox = T
                    )
qcNWDIRast <- nrastQc$getRaster()

thresh <- 0.1
dfClass <- data.frame( lower=c(-1, thresh),
                       upper=c(thresh,1),
                       class=c(0,1))

qcNWDIRastReclass <- raster::reclassify(qcNWDIRast,
                   dfClass)

qcNWDIRast %>% plot
qcNWDIRastReclass %>% plot
```




```{r}

#https://www.linkedin.com/pulse/ndvi-ndbi-ndwi-calculation-using-landsat-7-8-tek-bahadur-kshetri/

nrastQc <- neighRasterQc$new(  rasterPath =  here::here("Data","GeoData","Raster", "NWDI", "qcMNWDICropped4326.tif" ),
                    neighPath =here::here("Data","GeoData","QuebecNeighbourhoods"),
                    useBbox = T
                    )
qcNWDIRast <- nrastQc$getRaster()

thresh <- 0.1
dfClass <- data.frame( lower=c(-1, thresh),
                       upper=c(thresh,1),
                       class=c(0,1))

qcNWDIRastReclass <- raster::reclassify(qcNWDIRast,
                   dfClass)

qcNWDIRast %>% plot
qcNWDIRastReclass %>% plot


writeRaster(qcNWDIRastReclass,
            here::here("Data","GeoData","Raster","NWDI","qcMNWDICroppedReclass4326.tif"))
```