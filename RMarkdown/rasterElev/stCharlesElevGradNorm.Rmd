---
title: "stCharlesGradient"
author: "Charles"
date: "August 26, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librairies
 
```{r, include=FALSE}

GeneralHelpers::loadAll()
library(tidyverse)
library(magrittr)
library(ggplot2)
 
 
library(sf)
 
library(here)
library(glue)

#Custom packages
library(Census2SfSp)
library(neighShp) 
library(gradientvectorfield)

```
 


```{r pressure, echo=FALSE,include=F}

buff <- 0.005
rastQcBbox <- neighRasterQc$new( rasterPath=here::here("Data","GeoData","Raster", glue::glue("stcharlesmainelev_buff{buff}.tif")),
                                  useBbox = T)

myNeighGrad <- gradientvectorfield::neighRasterGradient$new(neighRaster = rastQcBbox,
                                                            strFileName = glue::glue("stCharlesMainGradElevNorm_buff{buff}"))

rastGradNormQc <- myNeighGrad$getRaster()


```
#Write the raster

```{r}

writeRaster(rastGradNormQc, here::here("Data","GeoData","Raster", glue::glue("stCharlesRiverGradNorm_buff{buff}.tif")))


```

```{r}

hydroStCharles <- hydroShpSaintCharlesOnlyMain$new()
shpLakesRivers <- hydroStCharles$getShpLakesRivers()

```



```{r}
 

png(here::here("Figures",glue::glue("stCharlesRiverGradNorm_buff{buff}.png")))

plot(rastGradNormQc, main="St-Charles River with elevation gradient norm")
plot(shpLakesRivers$geometry, add=T,col="blue")




dev.off()


```
