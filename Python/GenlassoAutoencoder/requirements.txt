absl-py==0.7.1
affine==2.2.2
astor==0.8.0
attrs==19.1.0
backcall==0.1.0
bleach==3.1.0
certifi==2019.6.16
Click==7.0
click-plugins==1.1.1
cligj==0.5.0
cycler==0.10.0
decorator==4.4.0
defusedxml==0.5.0
entrypoints==0.3
eyeD3==0.8.10
gast==0.2.2
google-pasta==0.1.7
grpcio==1.22.0
h5py==2.9.0
hmmlearn==0.2.2
ipykernel==5.1.1
ipython==7.6.1
ipython-genutils==0.2.0
jedi==0.14.1
Jinja2==2.10.1
joblib==0.13.2
json5==0.8.5
jsonschema==3.0.1
jupyter-client==5.3.1
jupyter-core==4.5.0
jupyterlab==1.0.2
jupyterlab-server==1.0.0
Keras==2.2.4
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
kiwisolver==1.1.0
Markdown==3.1.1
MarkupSafe==1.1.1
matplotlib==3.1.1
mistune==0.8.4
mkl-fft==1.0.14
mkl-random==1.0.2
mkl-service==2.3.0
nbconvert==5.5.0
nbformat==4.4.0
networkx==2.3
notebook==5.7.8
numpy==1.16.5
olefile==0.46
pandas==0.24.2
pandocfilters==1.4.2
parso==0.5.1
patsy==0.5.1
pexpect==4.7.0
pickleshare==0.7.5
Pillow==6.1.0
prometheus-client==0.7.1
prompt-toolkit==2.0.9
protobuf==3.9.0
ptyprocess==0.6.0
pyAudioAnalysis==0.2.5
pydub==0.23.1
Pygments==2.4.2
pyparsing==2.4.2
pyrsistent==0.15.3
python-dateutil==2.8.0
python-magic==0.4.15
pytz==2019.1
PyYAML==5.1.1
pyzmq==18.0.2
rasterio==1.0.21
scikit-learn==0.21.2
scipy==1.3.1
seaborn==0.9.0
Send2Trash==1.5.0
simplejson==3.16.0
six==1.12.0
sklearn==0.0
snuggs==1.4.6
statsmodels==0.10.0
tensorboard==1.14.0
tensorflow==1.14.0
tensorflow-estimator==1.14.0
termcolor==1.1.0
terminado==0.8.2
testpath==0.4.2
tornado==6.0.3
traitlets==4.3.2
Wave==0.0.2
wcwidth==0.1.7
webencodings==0.5.1
Werkzeug==0.15.4
wrapt==1.11.2
