
import os
import numpy as np
from PIL import Image
import pandas as pd


from GenlassoAutoencoder.definitions import *



def readImage(city="Mtl", strNeigh="Poin_Sain_Vill"):

    """Read raster image from dicretory using PIL.Image
    
    Param:
        city: str (city string id)
        strNeigh: str (string for list of neighbourhoods)
    """

    rastPath=os.path.join(ROOT_DIR,"Data","GeoData","Raster", "DEM",f"{city}{strNeigh}Elev.tif")
    rast = Image.open(rastPath)
              
    numPixels=np.prod(rast.size)
    print(f"There are {numPixels} pixels - dim - {rast.size} in the image {rastPath}" )
   
    return(rast)



def readNpArray(city="Mtl", strNeigh="Poin_Sain_Vill"):
    rast=readImage(city, strNeigh )
    arr=np.array(rast) 
    
    return(arr)

def readTensor(city="Mtl", strNeigh="Poin_Sain_Vill"):
    arr=readNpArray(city, strNeigh )
    tens=arr[np.newaxis, ...]
    
    return(tens)

 