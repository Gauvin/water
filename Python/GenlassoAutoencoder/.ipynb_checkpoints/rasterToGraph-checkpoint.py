
import networkx as nx
import numpy as np
import pandas as pd
 

def getEdgeList(rast):
    
    """Get the edge incidence matrix from a raster image 
    
    Param:
        rast: image 
    Return:
        edge list : pd dataframe where each row corresponds to an edge with from and to and the nodes are in the (i,j) format where i and j are the coordinates in the plane
    
    """
        #Create a regular lattice graph
    #invert the raster dimensions 
    G = nx.grid_2d_graph(rast.size[1],rast.size[0])
    
    
    numPixels=np.prod(rast.size)
    assert(numPixels == len(G.nodes()) )
    
    dfEdges=pd.DataFrame( { "start": [ u for u,v, dat in G.edges(data=True)],
                           "end": [ v for u,v, dat in G.edges(data=True)] } )

    
    heightGraph=max( [ max( row["start"][0], row["end"][0]) for k, row in  dfEdges.iterrows() ] )
    widthGraph=max( [ max( row["start"][1], row["end"][1]) for k, row in  dfEdges.iterrows() ] )
    
    #Quick check
    if( heightGraph+1 != rast.size[0] | widthGraph+1 != rast.size[0]):
        raise Exception(f"Fatal error in getEdgeList, the dimensions of the raster and the graph do not match!")
    
    print(f"There are {dfEdges.shape[0]} edges in the lattice graph")
    
    return(dfEdges)