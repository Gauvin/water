from keras.layers import Input, Dense, Layer
from keras.models import Model
from keras.constraints import Constraint
from keras.regularizers import Regularizer

from keras import backend as K
from keras import regularizers

import tensorflow as tf
from tensorflow import keras




class ElementWiseMult(tf.keras.layers.Layer):

    """Custom layer => one parameter per pixel (and not pixel X pixel parameters)
    
    use point-wise (pixel-wise) multiplication instead of regular matrix multiplication
    
        
    Used to solve the following problem:
    
        min_beta sum_i ||x_i - beta_i x_i|| + lambda sum_(i,j) \in E |beta_i - beta_j| 
        where 
            -i is the pixel index 
            -E is the set of edges between pairs of adjacent pixels we want to consider
            -lambda > 0 is the weight of the L1 norm regularization
    
        min_beta sum_i ||x_i - beta_i x_i||  is equivalent to solving min_beta sum_i ||x_i - beta_i'|| since we can always set  beta_i' = beta_i x_i
        however, with the L1 term lambda sum_(i,j) \in E |beta_i - beta_j|  both problems will not yield the same solution and the interpretation is also different
    
    """
    
    def __init__(self,  kernel_regularizer=None, **kwargs):
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        
        print(self.kernel_regularizer)
        super(ElementWiseMult, self).__init__(**kwargs)
        

    def build(self, input_shape):

        print(f'Here is the dim{input_shape}')
        
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(int(input_shape[-2]),int(input_shape[-1])),
                                      initializer='uniform',
                                      trainable=True,
                                      regularizer=self.kernel_regularizer)
        self.output_dim= input_shape[1]
        
        
        super(ElementWiseMult, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        return tf.multiply(x,      self.kernel )
    
    def compute_output_shape(self, input_shape):
        return (input_shape[-1], self.output_dim)
    

    


class PiecewiseConstant(tf.keras.layers.Layer):

    """Custom layer => one parameter per pixel (and not pixel X pixel parameters)
    
    do not multiply => only use the input dimension to build a weight matrix
    
    use to solve the following problem: 
    
        min_beta sum_i ||x_i - beta_i || + lambda sum_(i,j) \in E |beta_i - beta_j| + nu sum_i |beta_i|
        where 
            -i is the pixel index 
            -E is the set of edges between pairs of adjacent pixels we want to consider
            -lambda > 0 is the weight of the L1 norm regularization
            -nu > 0
    """
    
    def __init__(self,  kernel_regularizer=None, **kwargs):
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        
        print(self.kernel_regularizer)
        super(PiecewiseConstant, self).__init__(**kwargs)
        

    def build(self, input_shape):

        print(f'Here is the dim{input_shape}')
        
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(int(input_shape[-2]),int(input_shape[-1])),
                                      initializer='uniform',
                                      trainable=True,
                                      regularizer=self.kernel_regularizer)
        self.output_dim= input_shape[1]
        
        
        super(PiecewiseConstant, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
         
        #return( K.multiply( self.kernel, K.ones(K.shape(x)[0],K.shape(x)[1]) ) )
        return( self.kernel )
            
        #k = self.kernel
           
        #return( k[tf.newaxis, ...]) # f** this m** piece of s* dimensions never match
    
    
    def compute_output_shape(self, input_shape):
        return (input_shape[-1], self.output_dim)
    
 