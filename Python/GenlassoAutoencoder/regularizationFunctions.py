import numpy as np
import tensorflow as tf
from keras.regularizers import Regularizer
from keras import backend as K


from GenlassoAutoencoder.rasterToGraph import *



class fused_l1_with_reg_lasso_cls(Regularizer):
    """Regularizer for fused lasso with regular L1 lasso
    """

    def __init__(self, img, penWeightLassoFused=0, penWeightLassoStd=100):
        self.penWeightLassoFused = K.cast_to_floatx(penWeightLassoFused)
        self.penWeightLassoStd = K.cast_to_floatx(penWeightLassoStd)

        self.edgeList = getEdgeList(img)
        
    def __call__(self, x):
 
        print("In the fused_l1_with_reg_lasso_cls.__call__fct")
        regularization = fused_l1_with_reg_lasso(x,  
                                                 self.edgeList,
                                                 penWeightLassoFused=self.penWeightLassoFused ,
                                                 penWeightLassoStd=self.penWeightLassoStd)

        return regularization 
    
    

def fused_l1_with_reg_lasso(weightMat, edgeList, penWeightLassoFused= 1000, penWeightLassoStd = 1000 ):
    
    """Generic fused lasso regularization fct
    
    Given graph G=(V,E) where the nodes V represent the variables/pixels and the edges E codifies the adjacency structure
    
    param:
        weightMat: tensor
        penWeightLassoFused: int : penalty associated with fused lasso term sum_{(i,j) \in E} Beta_i - Beta_j
        penWeightLassoStd: int : penalty associated with regular variable selection lasso: sum_{i \in V} Beta_i
    output:
        regularization: double regularization penalty
        
    """
    
    if( min(penWeightLassoFused,penWeightLassoStd ) < 0):
        raise Exception("Fatal error, use non-negative penalty weights!")
    
 
 
    regularization = 0.
    
    #Fused lasso component
    if(penWeightLassoFused > 0):
        #easy matrix way => not sure this works, takes forever to compile the model
        #regularization +=  penWeightLassoFused * np.sum( abs( np.dot(incMatrix.todense() , arrFlatt) ) )
        
        #Otherwise, just take the edge list where each node represents a raster cell and has format (i,j) 
        #and penalize |beta_{ij} - beta_{kl} | if edge (i,j),(k,l) exists
        for k, row in edgeList.iterrows():
            if(k<=4):
                a=weightMat[ row["start"][0], row["start"][1] ]
                b=weightMat[ row["end"][0], row["end"][1] ]
                print( f"start weightMat: {a} - end weightMat: {b}")
                
            regularization += penWeightLassoFused * K.abs( weightMat[ row["start"][0], row["start"][1] ] - weightMat[ row["end"][0], row["end"][1] ] )
                 
    #Std lasso component
    if(penWeightLassoStd > 0):
        regularization += penWeightLassoStd * K.sum( K.abs( weightMat )  )
 
    return regularization



#Simple wrappers
 
def get_fused_l1_with_reg_lasso_zeroFusedLasso(penWeightLassoStd=1000 ):
    def fused_l1_with_reg_lasso_zeroFusedLasso(weightMat, penWeightLassoFused=0, penWeightLassoStd=penWeightLassoStd):
        fused_l1_with_reg_lasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=penWeightLassoStd)
    return(fused_l1_with_reg_lasso_zeroFusedLasso)

def get_fused_l1_with_reg_lasso_zeroLasso(penWeightLassoFused=1000 ):
    def fused_l1_with_reg_lasso_zeroLasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=0):
        fused_l1_with_reg_lasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=penWeightLassoStd)
    return(fused_l1_with_reg_lasso_zeroLasso)


 
#manRegOnlyFused=get_fused_l1_with_reg_lasso_zeroLasso(penWeightLassoFused = 1000)
#manRegOnlyStd=get_fused_l1_with_reg_lasso_zeroFusedLasso(penWeightLassoStd = 1000)


def fused_l1_with_reg_lasso_wrapper( penWeightLassoFused, penWeightLassoStd , img ):
    return( fused_l1_with_reg_lasso_cls(penWeightLassoFused, penWeightLassoStd) )