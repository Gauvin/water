from PIL import Image
import numpy as np
from matplotlib.cm import *
from matplotlib.pyplot import imshow


def normalize(x):
    
    """Normalize to [0,1] using x -> (x-min)/(max-min)
    
    """
    
    mn=np.min(x)
    mx=np.max(x)
    
    xNorm=1-((mx-x)/(mx-mn))
    
    return(xNorm)



def normalizePlotImg(img, fact=1):
    
    """Normalize a pixel image + change image resolution + apply blues color map
    
    params:
        img: PIL image instance
        fact: int : factor to use  resample/aggreagte the pixels for lower resolution to speed up the process
    
    out:
        i: PIL.Image instance
    """
    
    if(len(img.shape)>3 | len(img.shape)<2):
        raise("Fatal error, must input a tensor or dimension 2 or 3")
    
    if(len(img.shape)==3):
        normMat=normalize(img[0,:,:])
    else:
        normMat=normalize(img )
    
    #Normalize the matrix
    cmHot=get_cmap('Blues')
    
    #Get the color with the Blues color map
    predImgCM=cmHot(normMat)
    
    predImgCM=np.uint8(predImgCM*255)
    i=Image.fromarray(predImgCM)
    
    #Resize
    nx, ny= predImgCM.shape[0:2] #this is a width X heigh X rgba matrix
    if(int(np.ceil(ny*fact))<=0 | int(np.ceil(nx*fact))<=0):
        raise("Fatal error, cannot choose such a small factor for image resizing, must have at least 1 pixel!")
    i=i.resize( (int(np.ceil(ny*fact)), int(np.ceil(nx*fact) ) ) ) #transpose to go from raster/pixel grid to array
    
    i.show()
    
    return(i)
    
    
    
    
    
