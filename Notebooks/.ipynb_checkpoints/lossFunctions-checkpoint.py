import numpy as np
import tensorflow as tf


 

def fused_l1_with_reg_lasso(weightMat, penWeightLassoFused= 1000, penWeightLassoStd = 1000 ):
    
    """Generic fused lasso regularization fct
    
    Given graph G=(V,E) where the nodes V represent the variables/pixels and the edges E codifies the adjacency structure
    
    param:
        weightMat: tensor
        penWeightLassoFused: int : penalty associated with fused lasso term sum_{(i,j) \in E} Beta_i - Beta_j
        penWeightLassoStd: int : penalty associated with regular variable selection lasso: sum_{i \in V} Beta_i
    output:
        regularization: double regularization penalty
        
    """
    
    if( np.min(penWeightLassoFused,penWeightLassoStd ) < 0):
        raise("Fatal error, use non-negative penalty weights!")
    
    nx, ny=weightMat.shape
    arrFlatt=tf.reshape(weightMat, [-1] ) #nx*ny - flatten the matrix - ie transform mat for col vector
    
    regularization = 0.
    
    #Fused lasso component
    if(penWeightLassoFused > 0):
        regularization +=  penWeightLassoFused * abs( np.dot(incMatrix.todense(), arrFlatt) )
    
    #Std lasso component
    if(penWeightLassoStd > 0):
        regularization += penWeightLassoStd * abs( arrFlatt )
 
    return regularization



#Simple wrappers
 
def get_fused_l1_with_reg_lasso_zeroFusedLasso(penWeightLassoStd=1000 ):
    def fused_l1_with_reg_lasso_zeroFusedLasso(weightMat, penWeightLassoFused=0, penWeightLassoStd=penWeightLassoStd):
        fused_l1_with_reg_lasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=penWeightLassoStd)
    return(fused_l1_with_reg_lasso_zeroFusedLasso)

def get_fused_l1_with_reg_lasso_zeroLasso(penWeightLassoFused=1000 ):
    def fused_l1_with_reg_lasso_zeroLasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=0):
        fused_l1_with_reg_lasso(weightMat, penWeightLassoFused=penWeightLassoFused, penWeightLassoStd=penWeightLassoStd)
    return(fused_l1_with_reg_lasso_zeroLasso)


 
manRegOnlyFused=get_fused_l1_with_reg_lasso_zeroLasso(penWeightLassoFused = 1000)
manRegOnlyStd=get_fused_l1_with_reg_lasso_zeroFusedLasso(penWeightLassoStd = 1000)